### Q1

Tell me about a technical book you read recently, why you liked it, and why I
should read it.

### A1

Practical object-oriented design in Ruby: an agile prime, by Sandi Metz.

Despite being a relatively old book ( 2012), i like this book so far ( i’m 
still reading it) because it presents the concepts with clear language, no 
boring descriptions and It helped me to understand better some principles 
regarding object-oriented design.

I Think you should read it because even if you’re not familiar with 
object-oriented design you will understand quite easily this book. But if 
you’re already used to OOD you still should read it because it will probably 
show new points of view. Add to that the fact that you work on a company which 
is passionated about Ruby.

---

### Q2 

A service daemon in production has stopped responding to network requests. You
receive an alert about the health of the service, and log in to the affected 
node to troubleshoot. How would you gather more information about the process 
and what it is doing? What are common reasons a process might appear to be 
locked up, and how would you rule out each possibility?

### A2 

The very first things i’d check are the logs related to the failing service,
dmesg output and the system resources with tools like top, vmstat and iostat.
next thing i do is to open a second terminal window and restart the service 
while watching the log with tail -f <log_filename> . If i can’t get any clue
with this and i’m sure that there is enough system resources to keep the
service running I start to test each service that is related to the failing
service, because sometimes a service may fail because another resource is not
available and was not being monitored.

#### Common reasons are:

1 - out of memory : usually happens when a process needs to allocate more 
memory than it’s available. I either try to limit the amount of memory that 
the process can allocate or just add more memory. 

An example that i’ve faced on previous jobs: A client had a cache running on 
Squid and once a week he had to restart the service, he also had other 
concurrent services running on the same machine. The issue in this case was 
that a particular directive ( cache_mem ) was allowing to allocate almost the 
same amount of the installed memory.

2 - memory leaks: usually the community has patches to fix memory leaks issues
and applying these patches or updating the related packages will fix. 
Unfortunately not all the times there are patches available. This happened 
about a month ago with a friend and he called asking for help. The scenario 
was the following: no matter how much memory he added to the machine the 
service eventually stopped. Turns out that he was using a particular version 
of Ruby and Puma as at the moment we couldn’t update neither Ruby neither Puma 
because it was on production and we weren’t sure if the leak was caused by his 
code or not, we used the gem puma_worker_killer as a workaround. This gem will 
basically kill a worker if its process gets too large, and no more 500 errors 
for the customers.

3 - out of disk space : Happened at least one time at every company that i'́ve 
already worked for. Log files can get really large, and they will... so I use 
log rotate to not let they grow too much. In another scenario where log files 
are not the issue, I would check if there are any unnecessary files lying 
around and move it somewhere else.

4 - High CPU load: Sometimes the cpu load is so high that some process became 
locked up, the first step is to identify what is causing such high load and to 
do that i use tools like top or htop and sort it to display the processes 
which are using the most cpu power first. So i can decide if i’m just going to 
kill the process(es) or not because this can either be an attack or just too 
much people trying to use the service ( a web server for example).

5 - High I/O load: If the I/O load is high it will also increase the cpu load 
and the system may become unresponsive. I like to use tools like iostat, iotop 
and vmstat to isolate where the problem is if i have more than one storage 
device and to identify which process is causing the most I/O load. Two weeks 
ago a colleague asked for my help with the following scenario: Every 3 hours 
his application was stopping answering for some time, he was experiencing 
errors 500 on his web application, and the metrics reports showed I/O load 
spikes and also cpu load, did not mattered how many customers were using the 
application. I started by using iostat to find out which device was under high 
load. Then with iotop I discovered that mysql was the one to blame. At first 
didn’t made much sense because we had almost no customer using the app and 
almost no incoming connections, so i couldn’t be an attack. Then i noticed 
that he had many cron jobs scheduled to run every 3 hours and these jobs were 
basically doing bulk operations on mysql ( updates, import, etc). So i just 
had to adjust the time each task would start in order to avoid producing high 
loads at the same time.

---

### Q3

A user on an ubuntu machine runs `curl http://gitlab.com` Please describe in 
as much detail as you can the lifecycle of the command and what happens in the 
kernel, over the network, and on GitLab servers before the command completes.

### A3

1 - the first thing that will happen is the name resolving, in other words 
the host name GitLab.com is resolved to an IP address, by sending a query to 
a DNS server.

2 - Then with the IP address for the host curl wants to contact, it will 
open a TCP port sending a "connect request".the remote host will acknowledge 
the request and then the connection will be established and now it can be 
used to send a stream of data in both directions. When I started working on 
IT on 1998, This process was also known as 3 way hand shaking. 

3 - After established, curl will send a HTTP GET request.

4 - assuming that the GitLab.com web server accepts HTTP/1.1 requests, it 
keep a persistent connection open and will send a HTTP response that 
comprises: 

     - status line
     - response headers 
     - entity ( payload and/or readable ascii characters)

5 - after curl receives the response it will render to the default output 
device and the tcp/ip session is closed.

At Kernel level there are three system calls that send data over the network.
they start at the session layer and basically the path is:

     - write memory data to a file descriptor
     - send memory data to a socket 
     - send a message to a socket

the Kernel will check permissions and then forward the message to the next 
layer.

All the data sent to GitLab.com will observe the following flow: 

```Session -> Transport -> Network -> Link```  

At the transport layer, the kernel will try to find available space in 
common data structure used by all network related queues and buffers, copy
the data from user space to this data structure space, the TCP queue is 
activated, packets are sent with a TCP header and the control is passed over 
to the network layer. 

At network layer, the IPv4 ( or 6) is created, then happens the network 
filtering, routing and finally resulting in a destination object. 

At Link layer the main function of the kernel is to schedule the packets to 
be sent out at this layer the network device finally starts transferring the 
buffer.


After leaving the network device the data will travel, hoping from router to 
router until it finds its destination, GitLab in this case.     

All the data received by GitLab.com will observe the following flow: 

```Link -> Network -> Transport -> Session``` 

Then the web server will reply with the response described at the step 4. 
This response will basically follow through the same Transmission flow 
described before. 

*Transmission flow:  
Session -> Transport -> Network -> Link  

*Receive flow:
Link -> Network -> Transport -> Session

---

### Q4
One of the current challenges we are facing lies in storage capacity. Describe 
in as much detail as you can how you would face this challenge, where do you 
think the main bottlenecks could be and finally what actions would you take to 
understand the problem and to finally deliver an infrastructure that would 
support our growth.

### A4

I understand that dealing with large storages are a great responsibility and i 
would face this challenge with extra caution and sense of ownership. I would 
start gathering as much information as I can about the current infrastructure 
setup, asking around about any particular design decision that was made in the 
past. Then I would check the documentation, the system metrics, and 
statistical reports that could help me to estimate the future growth. In the 
question you mention storage capacity and i like to think that this is not 
limited to how much data you can store, but also, how fast the data can be 
manipulated and, of course, how safe is the data from a disaster or service 
disrepute. I’m considering storage capacity, performance and availability/
fault tolerance.

I think that the main bottlenecks are:

   – Hardware hard limits.
   – Management overhead.
   – Durability issues: a drive failure in a RAID group may degrade the 
performance.
   – Network.
   – Insufficient cpu power to process the I/O.
   – I would like to add two more bottlenecks : OPEX/CAPEX limitations and 
vendors delivery time.

I think a good approach for GitLab would be instead of just scalling-up, also
scale-out by building a storage cluster, using Openstack Ceph for example, 
because you can just add more nodes to the cluster when required, and when 
you're adding more nodes you're not only adding more capacity but also adding 
more processing power and I/O bandwidth.

If possible i would use object storage ( Ceph, Scality) to reduce the 
management overhead because i won’t have to bother tracking each mounted 
volume and by using an object store, you’re not limited to the amount of space 
each units holds, Nodes can be added easily in this approach.

object store may not be the best solution for everyone, but at this moment these
are just assumptions.  

---

### Q5

Write a program, topN, that given an arbitrarily large file and a number, N, 
containing individual numbers on each line (e.g. 200Gb file), will output the 
largest N numbers, highest first. Tell me about the run time/space complexity 
of it, and whether you think there's room for improvement in your approach.

### A5

I hope that I don’t sound rude or lame because I intend only to explain why i’m 
not exactly answering this one on a conventional way. 

In more than 15 years working on IT Operations, I never had to deal with
anything using the so called big O notation and time/space complexity. I can’t 
relate right now where it would fit on the production engineer’s duties. These 
are concepts that i really don’t understand at the moment but i’m willing to 
learn soon. I graduated in 2006 and I really can’t remember from the classes if 
i went through these subjects or not.

I'm pretty sure that if I search on Google I will find an answer for this 
question but I really don’t feel it is right, It’s not honest and won’t help 
improve myself for sure. I rather suffer the consequences of not delivering 
what you’re expecting than just show someone else’s answer and pretend that 
it’s mine.

What i can offer at the moment is how I would possible break down the problem, 
so i would appreciate if you consider at least this part. 

It's a huge file so probably i won't have enough memory to read all the file 
at once. I have to read smaller parts of the given file, sort it in the 
descending order, and store the result at an intermediate file. I will keep 
repeating these steps until i process the whole file. Then the last step would 
be to compare, sorting between them, and merge all the intermediate files. 
After that i would just read the first N lines and output the result.

I think that splitting and sorting, will consume less time because it will do 
much less comparissons or at least they will finish to process more faster
than just trying to read the entire file and sort it. 

As i'm using intermediate files to store small chunks of data, I'm using 
more disk space, so i think that this would be something that needs to be 
improved.

---


### Other questions related to your candidacy

On what kind of timeline are you looking for a new job?

A: I started looking for a new job about 2 months ago, because my contract is 
coming to an end at December 20 and I'm not sure if i want to renew it or not 
just because that would require me to relocate to a place that i really dislike.
I'll be available in two weeks from now, for both full-time and part-time 
positions, remotely or not. 

Do you have remote working experience?

A: Yes! I'm quite used to this kind of job as i'm already working remotely for 2 
years or more. In fact my current job and my last two were remote work. 

Do you have an open source project that you own or contributed to that you 
feel particularly proud about? could you provide a link to it or send a code 
sample?

A: This is not a project, not even an impressive piece of work but i'm 
particularly proud about it because it's useful, simple, and helped some fellow 
colleagues to understand that they're not tied to bash scripts as Ruby can be 
used as a good replacement.

Here is the link: 
https://github.com/rafaelthedevops/isp_scripts/blob/master/checkbanda
